import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  box-sizing: bordex-box;
}
html, body, #root {
  max-height: 100vh;
  max-width: 100vw;
  width: 100%;
  height: 100%;
}
*, button, input {
  border: 0;
  background: 0;
  font-family: 'Montserrat Alternates', -apple-system, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  color: var(--darker-gray);
  outline: 0;
}
html {
  font-size: 16px;
  background:
  radial-gradient(circle farthest-side at 0% 50%,var(--alpha) 23.5%,rgba(240,166,17,0) 0)21px 30px,
  radial-gradient(circle farthest-side at 0% 50%,#1186AD 24%,rgba(240,166,17,0) 0)19px 30px,
  linear-gradient(var(--alpha) 14%,rgba(240,166,17,0) 0, rgba(240,166,17,0) 85%,var(--alpha) 0)0 0,
  linear-gradient(150deg,var(--alpha) 24%,#1186AD 0,#1186AD 26%,rgba(240,166,17,0) 0,rgba(240,166,17,0) 74%,#1186AD 0,#1186AD 76%,var(--alpha) 0)0 0,
  linear-gradient(30deg,var(--alpha) 24%,#1186AD 0,#1186AD 26%,rgba(240,166,17,0) 0,rgba(240,166,17,0) 74%,#1186AD 0,#1186AD 76%,var(--alpha) 0)0 0,
  linear-gradient(90deg,#1186AD 2%,var(--alpha) 0,var(--alpha) 98%,#1186AD 0%)0 0 var(--alpha);
  background-size: 40px 60px;
}
:root {
  --alpha: #118AB2;
  --beta: #FFD166;
  --gamma: #EF476F;
  --delta: #06D6A0;
  --epsilon: #073B4C;
  --gray: #7A7A7A;
  --dark-gray: #444444;
  --darker-gray: #212121;
  --light-gray: #D3D3D3;
  --black: #1F1F1F;
  --white: #E5E5E5;
}
`;

export const theme = () => ({
  bp: {
    mobileMin: 320,
    mobileMax: 767,
    tabletMin: 768,
    tabletMax: 1023,
    desktopMin: 1024,
  },
});
