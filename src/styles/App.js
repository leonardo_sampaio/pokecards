import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  max-width: ${(props) => props.theme.bp.desktopMin}px;
  height: 100%;
  margin: 0 auto;
`;

export const Main = styled.main`
  max-width: 100%;
`;
