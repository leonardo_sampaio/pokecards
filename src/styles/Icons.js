export { ArrowBackCircle, SearchCircle } from "@styled-icons/ionicons-solid";
export {
  SearchAlt2,
  Fullscreen,
  ExitFullscreen,
} from "@styled-icons/boxicons-regular/";
export { CloseCircleOutline } from "@styled-icons/evaicons-outline/";
