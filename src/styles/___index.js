import styled, { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";

export const theme = {
  color: {
    blue: "#0512FF",
    darkBlue: "#0009B3",
    lightBlue: "#129AFF",
    yellow: "#FFCD05",
    darkYellow: "#B38F02",
    darkGray: "#444444",
  },
  responsive: {
    mobile: 400,
    phablet: 550,
    tablet: 750,
    desktop: 1000,
    desktopHD: 1200,
  },
};

export const GlobalStyle = createGlobalStyle`
  ${normalize}
  
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: none;
  }
  li { list-style:none;}

  html, body, #root {
    height: 100%;
  }
  body {
    -webkit-font-smoothing: antialiased !important;
    font-family: 'Montserrat Alternates', sans-serif;
    font-size: 16px;
    background-color: ${(props) => props.theme.color.lightBlue};
  }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 800px;
  margin: 0 auto;
  // padding: 0 24px;

  @media (max-width: 400px) {
    height: 100%;
    // overflow-y: hidden;
  }
`;

export const StyledHeader = styled.header`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 16px 0;

  a {
    font-family: "Bangers", cursive;
    font-size: 48px;
    font-weight: bold;
    text-decoration: none;
    color: ${(props) => props.theme.color.yellow};
    // text-shadow: 0px 0px 24px rgba(0, 0, 0, 0.25);
    text-shadow: -1.25px 0 black, 0 1.25px black, 1.25px 0 black,
      0 -1.25px black;
  }
`;

export const StyledSearchBar = styled.div`
  form {
    display: flex;
    align-items: flex-start;
    margin: 0 24px;

    label {
      flex-grow: 1;
      font-size: 0.8rem;
      line-height: 1rem;
      color: ${(props) => props.theme.color.yellow};

      input {
        display: block;
        height: 40px;
        width: 100%;
        border: 1px solid #aaa;
        border-right: none;
        border-top-left-radius: 8px;
        border-bottom-left-radius: 8px;
        padding: 8px;
        font-size: 1rem;
        line-height: 1rem;
      }
    }
    & > input {
      display: block;
      height: 40px;
      background-color: ${(props) => props.theme.color.yellow};
      color: ${(props) => props.theme.color.darkBlue};
      border-top-right-radius: 8px;
      border-bottom-right-radius: 8px;
      padding: 4px 8px;
    }
  }
`;

export const StyledCardList = styled.section`
  // display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: space-between;
  // overflow: hidden;
  flex: 1;
  padding: 24px 0 0;

  @media (max-width: ${(props) => props.theme.responsive.desktop}px) {
    justify-content: center;
    align-items: center;
  }

  .swiper-wrapper {
    padding: 24px 0 40px;
  }

  .loading {
  }
  .error {
  }
  .notFound {
  }
  .card {
    display: flex !important;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
    // min-width: 304px;
    border-radius: 8px;
    // background: #e6e6e6;
    // box-shadow: 5px 5px 10px #1365a1, -5px -5px 10px #177bc5;

    .id {
      display: none;
      min-height: 24px;
    }
    .name {
      display: none;
      min-height: 24px;
      font-size: 32px;
      color: ${(props) => props.theme.color.darkGray};
    }
    .image {
      max-width: 240px;
      max-height: 330px;
      // border: 8px solid ${(props) => props.theme.color.darkGray};
      border-radius: 10px;
      -webkit-box-shadow: -4px 4px 12px 0px rgba(0,0,0,0.25);
      -moz-box-shadow: -4px 4px 12px 0px rgba(0,0,0,0.25);
      box-shadow: -4px 4px 12px 0px rgba(0,0,0,0.25);
    }
    .types {
      display: none;
      min-height: 24px;
    }
    & > a {
      display: none;
    }
  }
`;

export const StyledCardDetail = styled.div``;

export const StyledAttackDetailModal = styled.div`
  display: flex;
  flex-direction: column;
`;
