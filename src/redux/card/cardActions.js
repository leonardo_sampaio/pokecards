import axios from "axios";

import {
  FETCH_CARDS_REQUEST,
  FETCH_CARDS_SUCCESS,
  FETCH_CARDS_FAILURE,
} from "./cardTypes";

const CARDS_API_URL = "https://api.pokemontcg.io/v1";

export const fetchCardsRequest = () => ({
  type: FETCH_CARDS_REQUEST,
});

export const fetchCardsSuccess = (cards) => ({
  type: FETCH_CARDS_SUCCESS,
  payload: cards,
});

export const fetchCardsFailure = (error) => ({
  type: FETCH_CARDS_FAILURE,
  payload: error,
});

export const fetchCards = () => {
  return (dispatch) => {
    dispatch(fetchCardsRequest());
    axios
      .get(`${CARDS_API_URL}/cards`)
      .then((response) => {
        const cards = response.data.cards;
        dispatch(fetchCardsSuccess(cards));
      })
      .catch((error) => {
        const errorMsg = error.message;
        dispatch(fetchCardsFailure(errorMsg));
      });
  };
};

export const searchCards = (searchQuery) => {
  return (dispatch) => {
    dispatch(fetchCardsRequest());
    axios
      .get(`${CARDS_API_URL}/cards?name=${searchQuery}`)
      .then((response) => {
        const cards = response.data.cards;
        dispatch(fetchCardsSuccess(cards));
      })
      .catch((error) => {
        const errorMsg = error.message;
        dispatch(fetchCardsFailure(errorMsg));
      });
  };
};
