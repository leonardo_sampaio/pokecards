import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { searchCards } from "../../redux";

import { Form, SearchIcon } from "./styles";

export default function SearchBar() {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleSubmit = (e) => {
    const searchQuery = e.target[0].value;
    if (searchQuery) {
      dispatch(searchCards(searchQuery));
      history.push("/");
    }
  };

  return (
    <Form
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit(e);
      }}
    >
      <input name="search" type="search" placeholder="Search card name" />
      <button>
        <SearchIcon />
      </button>
    </Form>
  );
}
