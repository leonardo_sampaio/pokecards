import styled from "styled-components";

import { SearchCircle } from "../../styles/Icons";

export const Form = styled.form`
  display: flex;
  width: 100%;
  max-width: min(80vw, 500px);
  height: 2.8rem;
  /* margin: 1rem auto; */
  padding: 0 0 0 1rem;
  background: var(--light-gray);
  border-radius: 2rem;
  box-shadow: inset -2px -2px var(--epsilon);

  input {
    display: flex;
    flex: 1;
    color: var(--epsilon);
    font-size: 1rem;
  }

  button {
    width: 2.667rem;
    height: 2.667rem;
    cursor: pointer;
    /* background: var(--epsilon); */
    border-radius: 2rem;
    margin-right: 2px;
    /* border-top-right-radius: 2rem;
    border-bottom-right-radius: 2rem; */
  }
`;

export const SearchIcon = styled(SearchCircle)`
  width: 2.667rem;
  height: 2.667rem;
  fill: var(--epsilon);
`;
