import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import SwiperCore, { Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import { Container, Backdrop } from "./styles";
import "swiper/swiper.scss";
import "swiper/components/pagination/pagination.scss";

SwiperCore.use([Pagination]);

export default function CardList(props) {
  const cardData = useSelector((state) => state.card);
  console.log("cardData", cardData);
  const { cards, loading, error } = cardData || {};

  return (
    <Container>
      {loading ? (
        <div className="loading">Loading cards...</div>
      ) : error ? (
        <div className="error">Error loading cards</div>
      ) : cards && cards.length === 0 ? (
        <div className="notFound">Card not found</div>
      ) : cards && cards.length > 0 ? (
        <Swiper
          pagination={{ dynamicBullets: true }}
          slidesPerView={1}
          slidesPerColumnFill={"row"}
          cssMode={true}
          breakpoints={{
            768: {
              slidesPerView: 2,
              slidesPerColumn: 2,
              spaceBetween: 10,
              cssMode: false,
            },
            1024: {
              slidesPerView: 3,
              slidesPerColumn: 2,
              spaceBetween: 10,
              cssMode: false,
            },
            1200: {
              slidesPerView: 4,
              slidesPerColumn: 2,
              spaceBetween: 10,
              cssMode: false,
            },
          }}
        >
          {cards.map((card) => (
            <SwiperSlide key={card.id}>
              <Link className="card" to={card.id}>
                <img className="image" alt={card.name} src={card.imageUrl} />
                <span className="id">ID: {card.id}</span>
                <h2 className="name">{card.name}</h2>
                <ul className="types">
                  {card.types &&
                    card.types.map((type, i) => <li key={i}>{type}</li>)}
                </ul>
                <Backdrop className="backdrop" />
              </Link>
            </SwiperSlide>
          ))}
        </Swiper>
      ) : null}
    </Container>
  );
}
