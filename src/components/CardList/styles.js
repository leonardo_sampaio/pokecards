import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  width: 100%;
  flex-flow: row wrap;
  justify-content: center;
  align-items: space-between;
  flex: 1;
  padding: 24px 0 0;

  .swiper-slide {
    height: auto;
  }

  @media (max-width: ${(props) => props.theme.bp.desktopMin}px) {
    justify-content: center;
    align-items: center;
  }

  .swiper-wrapper {
    padding: 1.5rem 0;
  }

  @media (max-width: ${(props) => props.theme.bp.desktopMin}px) {
    .swiper-wrapper {
      padding: 0;
    }
  }
  .swiper-pagination-bullet-active {
    background-color: var(--epsilon);
  }

  .loading {
  }
  .error {
  }
  .notFound {
  }
  .card {
    position: relative;
    /* height: 330px; */
    display: flex !important;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
    border-radius: 8px;
    text-decoration: none;

    .image {
      width: 240px;
      height: 330px;
      border-radius: 10px;
      -webkit-box-shadow: -4px 4px 12px 0px rgba(0, 0, 0, 0.25);
      -moz-box-shadow: -4px 4px 12px 0px rgba(0, 0, 0, 0.25);
      box-shadow: -4px 4px 12px 0px rgba(0, 0, 0, 0.25);
    }
    .id {
      display: flex;
      justify-content: center;
      align-items: center;
      min-height: 13px;
      margin: 1rem 0 0.5rem;
      padding: 0 0.5rem;
      background: var(--gray);
      color: var(--white);
      border-radius: 1rem;
      font-size: 0.667rem;
      font-weight: bold;
    }
    .name {
      min-height: 39px;
      margin-bottom: 1rem;
      font-size: 2rem;
      color: var(--epsilon);
      text-align: center;
    }
    .types {
      min-height: 24px;
      li {
        list-style: none;
        padding: 0 0.5rem;
        background: var(--gamma);
        border-radius: 1rem;
      }
    }

    @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
      max-width: 240px;
      margin: 0 auto;

      .id,
      .name,
      .types {
        z-index: 3;
        display: none;
        position: absolute;
        max-width: 100%;
        text-align: center;
        color: var(--white);
        &.id {
          top: 1%;
        }
        &.name {
          top: 40%;
        }
        &.types {
          bottom: 15%;
        }
      }

      &:hover {
        .backdrop,
        .id,
        .name,
        .types {
          display: block;
        }
      }
    }
  }
`;

export const Backdrop = styled.div`
  @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
    display: none;
    z-index: 2;
    width: 100%;
    height: 100%;
    border-radius: 10px;
    background: var(--black);
    opacity: 0.9;
    position: absolute;
    top: 0;
    left: 0;
  }
`;
