import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

import AttackDetail from "../AttackDetail";

import { Container, FullscreenIcon, Backdrop } from "./styles";

export default function CardDetail() {
  const [attackDetail, setAttackDetail] = useState({});
  const { id: cardId } = useParams();
  const card =
    useSelector((state) =>
      state.card.cards.filter((c) => c.id === cardId)
    )[0] || {};

  function selectAttack(attack, card) {
    setAttackDetail({
      ...attack,
      resistances: card.resistances,
      weaknesses: card.weaknesses,
    });
  }

  function handleCloseModal() {
    setAttackDetail({});
  }
  function handleFullscreen() {
    console.log("handleFullscreen");
    document.querySelector(".imgWrapper").classList.toggle("fs");
  }

  return (
    <Container>
      <div className="info">
        <div className="upper-info">
          <span className="id">ID: {card.id}</span>
          {card.types &&
            card.types.map((type, i) => (
              <span key={i} className="type">
                {type}
              </span>
            ))}
        </div>
        <h2>{card.name}</h2>

        <div className="attacks">
          {card.attacks ? <span>Attacks:</span> : null}
          {card.attacks &&
            card.attacks.map((attack, i) => (
              <span
                className="attack"
                key={i}
                onClick={() => selectAttack(attack, card)}
              >
                {attack.name}
              </span>
            ))}
        </div>
      </div>
      <div className="imgWrapper" onClick={() => handleFullscreen()}>
        <img alt={card.name} src={card.imageUrlHiRes} />
        <FullscreenIcon />
        <Backdrop />
      </div>
      {Object.keys(attackDetail).length > 0 ? (
        <AttackDetail attack={attackDetail} closeFn={handleCloseModal} />
      ) : null}
    </Container>
  );
}
