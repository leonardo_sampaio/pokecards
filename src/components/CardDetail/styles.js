import styled from "styled-components";

import { Fullscreen, ExitFullscreen } from "../../styles/Icons";

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  /* width: max(240px, calc(100vw - 2rem)); */
  /* min-width: 240px; */
  background: var(--epsilon);
  border-radius: 0.5rem;
  margin: 1rem 0 0;
  padding: 0 0 1rem;

  .info {
    display: flex;
    flex-flow: column nowrap;
    width: 100%;
    .upper-info {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin: 0.5rem;
    }
  }
  .id {
    height: 1rem;
    line-height: 1rem;
    font-size: 0.667rem;
    font-weight: bold;
    background: var(--delta);
    padding: 0 0.5rem;
    border-radius: 0.125rem;
  }
  .type {
    background: var(--gray);
    padding: 0.125rem 0.5rem;
    border-radius: 1rem;
    font-size: 0.9rem;
  }
  h2 {
    margin: 0.5rem 0 1rem;
    font-size: 2rem;
    color: var(--white);
    line-height: 1.25rem;
    text-align: center;
  }
  .attacks {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-around;
    background: var(--darker-gray);
    /* padding: 0.25rem 1rem 1rem; */

    span:first-child {
      display: flex;
      flex-basis: 100%;
      margin: 0.75rem 1rem;
      font-weight: bold;
      font-size: 0.8rem;
      font-style: italic;
      color: var(--white);
    }
    .attack {
      background: var(--white);
      color: var(--epsilon);
      font-weight: bold;
      border-radius: 0.5rem;
      padding: 0.5rem;
      margin-bottom: 1rem;
      cursor: pointer;
    }
  }
  img {
    /* width: 240px; */
    height: 240px;
    border-radius: 10px;
  }
  .imgWrapper {
    position: relative;
    height: 240px;
    margin-top: 1rem;
    cursor: pointer;
    &.fs {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 0;

      img {
        z-index: 3;
        height: auto;
        max-height: 80vh;
        max-width: 80vw;
      }
      div {
        display: block;
      }
      svg,
      path {
        display: none;
      }
    }
    &:hover {
      svg,
      div {
        display: block;
      }
    }
  }

  @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
    flex-flow: row-reverse nowrap;
    padding: 1rem;
    align-items: flex-start;

    .info {
      height: 100%;
      justify-content: space-between;
    }
    .imgWrapper {
      margin: 0;
    }
    .info {
      margin-left: 1rem;
    }
  }
`;

export const FullscreenIcon = styled(Fullscreen)`
  display: none;
  z-index: 3;
  width: 6rem;
  height: 6rem;
  fill: var(--white);
  position: absolute;
  top: calc(50% - 3rem);
  left: calc(50% - 3rem);
`;

export const ExitFullscreenIcon = styled(ExitFullscreen)`
  z-index: 3;
  width: 2.667rem;
  height: 2.667rem;
  fill: var(--white);
`;

export const Backdrop = styled.div`
  display: none;
  z-index: 2;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: var(--black);
  opacity: 0.5;
  border-radius: 9px;
`;
