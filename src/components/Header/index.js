import React from "react";
import { useHistory, useLocation } from "react-router-dom";

import { Container, BackIcon } from "./styles";

export default function Header() {
  const history = useHistory();
  const location = useLocation();
  const goBack = () => history.goBack();

  return (
    <Container>
      {location.pathname !== "/" ? (
        <button onClick={() => goBack()}>
          <BackIcon />
        </button>
      ) : null}
      <h1>Pokecards</h1>
    </Container>
  );
}
