import styled from "styled-components";

import { ArrowBackCircle } from "../../styles/Icons";

export const Container = styled.header`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  flex-shrink: 0;
  width: 100vw;
  height: 4rem;

  @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
    height: 8rem;
  }

  > button {
    position: absolute;
    top: 0;
    left: 0;
    width: 4rem;
    height: 4rem;
    padding: 0.5rem;
    outline: 0;
  }
  @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
    button {
      display: none;
    }
  }

  h1 {
    line-height: 4rem;
    font-family: "Bangers", sans-serif;
    font-size: 2.25rem;
    text-align: center;
    letter-spacing: 2.5px;
    color: var(--beta);
    text-shadow: 1.25px 0 black, -1.25px 0 black, 0 1.25px black,
      0 -1.25px black;
  }
  @media screen and (min-width: ${(props) => props.theme.bp.desktopMin}px) {
    h1 {
      font-size: 4rem;
    }
  }
`;

export const BackIcon = styled(ArrowBackCircle)`
  width: 2rem;
  height: 2rem;
  fill: var(--beta);
  background: var(--epsilon);
  border-radius: 50%;
`;
