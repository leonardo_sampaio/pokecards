import React from "react";

import { Container, CloseIcon } from "./styles";

export default function AttackDetail(props) {
  const { attack, closeFn } = props;

  return (
    <Container display={Object.keys(attack).length}>
      <CloseIcon onClick={() => closeFn()} />
      <span className="name">{attack.name}</span>
      <span className="text">{attack.text}</span>
      <span className="damage">Damage: {attack.damage}</span>
      <div className="cost">
        <span className="energyCost">Cost: {attack.convertedEnergyCost}</span>
        <ul>
          {attack.cost && attack.cost.map((cost, i) => <li key={i}>{cost}</li>)}
        </ul>
      </div>
      {attack.resistances ? (
        <div className="resistances">
          <span>Resistance: </span>
          <span>
            {attack.resistances[0].type} {attack.resistances[0].value}
          </span>
        </div>
      ) : null}
      {attack.weaknesses ? (
        <div className="weaknesses">
          <span>Weaknesses: </span>
          <span>
            {attack.weaknesses[0].type} {attack.weaknesses[0].value}
          </span>
        </div>
      ) : null}
    </Container>
  );
}
