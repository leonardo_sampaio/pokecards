import styled from "styled-components";

import { CloseCircleOutline } from "../../styles/Icons";

export const Container = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  flex-flow: column nowrap;
  width: 240px;
  top: calc(50% - 142px);
  left: calc(50% - 142px);
  background: var(--light-gray);
  border: 0.5rem solid var(--dark-gray);
  border-radius: 1rem;
  padding: 1rem;

  .name {
    font-size: 2rem;
    font-weight: bold;
    color: var(--epsilon);
    align-self: center;
  }
  .text {
    align-self: center;
    text-align: center;
    margin-top: 0.5rem;
  }
  .damage {
    font-weight: bold;
    border-top: 1px solid var(--epsilon);
    margin-top: 0.5rem;
    padding-top: 0.5rem;
    text-align: center;
  }
  .cost {
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-around;
    font-weight: bold;
    border-top: 1px solid var(--epsilon);
    margin-top: 0.5rem;
    padding-top: 0.5rem;
  }
  .resistances,
  .weaknesses {
    /* display: flex;
    flex-flow: row nowrap;
    justify-content: space-around; */
    font-weight: bold;
    margin-top: 0.5rem;
    padding-top: 0.5rem;
    text-align: center;
    &.resistances {
      margin-bottom: 0.5rem;
      border-top: 1px solid var(--epsilon);
    }
  }
`;
export const CloseIcon = styled(CloseCircleOutline)`
  z-index: 3;
  width: 1.5rem;
  height: 1.5rem;
  fill: var(--epsilon);
  align-self: flex-end;
  cursor: pointer;
`;
