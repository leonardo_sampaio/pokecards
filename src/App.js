import React, { useEffect } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { useDispatch } from "react-redux";
import { ThemeProvider } from "styled-components";

import { fetchCards } from "./redux";

import GlobalStyles, { theme } from "./styles/GlobalStyles";
import { Container } from "./styles/App";

import Header from "./components/Header";
import SearchBar from "./components/SearchBar";
import CardList from "./components/CardList";
import CardDetail from "./components/CardDetail";

export default function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCards());
    console.log("App.effect");
  });

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Container>
          <Header />
          <SearchBar />
          <Route exact path="/" component={CardList} />
          <Route path="/:id" component={CardDetail} />
        </Container>
      </Router>
      <GlobalStyles />
    </ThemeProvider>
  );
}
